<?php
/**
 * GymBeam s.r.o.
 *
 * Copyright © GymBeam, All rights reserved.
 *
 * @author Erik Kalinak <erik.kalinak@gymbeam.com>
 * @copyright Copyright © 2019  GymBeam (https://gymbeam.com/)
 * @category GymBeam
 */

namespace GymBeam\Debug\Helper;

use GymBeam\Backend\Block\System\Config\Renderer\Direction;
use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper
{
    public const CONFIG_BLOCK_INFO_ENABLE = 'dev/debug/block_info';

    /**
     * Return true in case code hints are enabled
     *
     * @return bool
     */
    public function isHintsInCodeEnabled(): bool
    {
        return (bool)$this->scopeConfig->getValue(self::CONFIG_BLOCK_INFO_ENABLE);
    }
}
