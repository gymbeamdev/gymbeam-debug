# GymBeam_Debug

## Description

**HTML debug info functionality**
By configuration, turns on _(or off)_ the display of container and block names and their templates in HTML comments.  

Composer Installation
Each of these commands should be run from the command line at the Magento 2 root.

### add this repository to your composer.json
$ composer config repositories.magento2-gymbeam-debug git https://gymbeam@bitbucket.org/gymbeamdev/gymbeam-debug.git

### require module
$ composer require gymbeam/debug:dev-master

### enable module
$ php -f bin/magento module:enable GymBeam_Debug
$ php -f bin/magento setup:upgrade

## Configuration
* Advanced > Developer > Debug
  * `dev/debug/block_info` - Enable / Disable tags with block name and template in source code ("HTML debug info" funcion)

## Dependencies / Relations

## Events & Plugins 
* Plugins
    * `gymbeamFrameworkViewLayout` - rendering containers _(and displays container names directly in HTML code)_
    * `gymbeamFrameworkViewElementAbstractBlock` - rendering of each block: if the debug function is enabled, it displays the name of the block and its template in HTML comments

## Changelog
* ver. 1.0.0
    * [Module created]
