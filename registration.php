<?php
/*
 * GymBeam s.r.o.
 *
 * Copyright © GymBeam, All rights reserved.
 *
 * @author David Chochol <david.chochol@gymbeam.com>
 * @copyright Copyright © 2021  GymBeam (https://gymbeam.com/)
 * @category GymBeam
 *
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'GymBeam_Debug',
    __DIR__
);
