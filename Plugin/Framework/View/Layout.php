<?php
/**
 * GymBeam s.r.o.
 *
 * Copyright © GymBeam, All rights reserved.
 *
 * @author Erik Kalinak <erik.kalinak@gymbeam.com>
 * @author K. Goč-Benka <kristian.goc-benka@gymbeam.com>
 * @copyright Copyright © 2019  GymBeam (https://gymbeam.com/)
 * @category GymBeam
 */

namespace GymBeam\Debug\Plugin\Framework\View;

use GymBeam\Debug\Helper\Config;

class Layout
{
    protected Config $configHelper;

    /**
     * Render constructor.
     * @param Config $configHelper
     */
    public function __construct(
        Config $configHelper
    ) {
        $this->configHelper = $configHelper;
    }

    /**
     * @param $subject
     * @param callable $proceed
     * @return string
     */
    public function aroundRenderNonCachedElement($subject, callable $proceed, $name)
    {
        $enabled = $this->configHelper->isHintsInCodeEnabled();
        if (!$enabled) {
            return $proceed($name);
        }

        $returnValue = '';

        if (!$subject->isUiComponent($name) && !$subject->isBlock($name)) {
            $returnValue .= "<!-- C: " . $name . " -->\n";
        }

        $returnValue .= $proceed($name);

        if (!$subject->isUiComponent($name) && !$subject->isBlock($name)) {
            $returnValue .= "\n<!-- C: " . $name . " | END -->";
        }

        return $returnValue;
    }
}
