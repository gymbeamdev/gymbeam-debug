<?php
/**
 * GymBeam s.r.o.
 *
 * Copyright © GymBeam, All rights reserved.
 *
 * @author Erik Kalinak <erik.kalinak@gymbeam.com>
 * @author K. Goč-Benka <kristian.goc-benka@gymbeam.com>
 * @copyright Copyright © 2019  GymBeam (https://gymbeam.com/)
 * @category GymBeam
 */

namespace GymBeam\Debug\Plugin\Framework\View\Element;

use GymBeam\Debug\Helper\Config;

class AbstractBlock
{
    protected Config $config;

    /**
     * Render constructor.
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    public function aroundToHtml($subject, callable $proceed)
    {
        $enabled = $this->config->isHintsInCodeEnabled();
        if (!$enabled) {
            return $proceed();
        }
        $blockName = 'B: ' . $this->getName($subject);
        $blockName .= $subject->getParentBlock() ? ' | ' . $this->getName($subject->getParentBlock()) : '';
        $block_template = ($subject->getTemplate() ? ' | ' . $subject->getTemplate() : '');
        $returnValue = "\n<!-- " . $blockName . $block_template . " -->";
        $returnValue .= $proceed();
        $returnValue .= "\n<!-- " . $blockName . " | END -->";
        return $returnValue;
    }

    /**
     * Return name of the block
     *
     * @param $subject
     * @return mixed
     */
    private function getName($subject)
    {
        return $subject->getNameInLayout() ?: str_replace(array('\Interceptor', '\\'), array('', '/'),
            get_class($subject));
    }
}
